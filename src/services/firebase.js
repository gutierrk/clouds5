// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth, GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import { getDatabase } from "firebase/database";
import { getFirestore } from "firebase/firestore";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDMVH-AHpKllDpoweWBnDYCoHxnhkiXO_Q",
  authDomain: "lab-5-78834.firebaseapp.com",
  databaseURL: "https://lab-5-78834-default-rtdb.firebaseio.com",
  projectId: "lab-5-78834",
  storageBucket: "lab-5-78834.appspot.com",
  messagingSenderId: "435913517479",
  appId: "1:435913517479:web:ef3191486271ede4466e82",
  measurementId: "G-JS775GHZ74"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const auth = getAuth();

const provider = new GoogleAuthProvider();
provider.setCustomParameters({ prompt: "select_account" });

export const signInWithGoogle = () => signInWithPopup(auth, provider);
export const db = getDatabase(app);
export const dbFirestore = getFirestore(app);